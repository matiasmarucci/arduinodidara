
#include <OneWire.h>
#include <ControlTemperature.h>
#include <DS2406.h>

class Dispositivos {
public:
    Dispositivos (OneWire*);
    ~Dispositivos ();
    void Begin (int16_t,int16_t,int16_t,int16_t,
                DeviceAddress,DeviceAddress,DeviceAddress,uint16_t, uint16_t,bool,bool,bool);
    bool IsPresent();
    void Enable();
    void Disable();
    void EnableSensor(const char*, bool);
    int16_t getTempDesde(const char*);
    int16_t getTempHasta(const char*);
    void setTempDesde(const char*, int16_t);
    void setTempHasta(const char*, int16_t);
    void getAddrRele(uint8_t* paddrRele);
    void getAddrSensor(uint8_t* paddrSensor, const char* Sensor);
    bool isSensorPresent(const char*);
    bool isSensorEnabled(const char*);
    void setTimeSensor(const char*, uint16_t);
    bool checkPresence();
    bool checkSensorPresence(const char*);
    uint16_t getTimeSensor(const char*);
    ControlTemperature SensorTempA;
    ControlTemperature SensorTempB;
    boolean SensorAactivado;
    boolean SensorBactivado;
    boolean EstadoA=false;
    boolean EstadoB=false;
    int16_t TempActualA;
    int16_t TempActualB;
    OneWireSwitch ctl2406;
private:
    boolean Conectado;
    boolean Activado;
    boolean SensorAPresente;
    boolean SensorBPresente;
    int16_t TempDesdeA;
    int16_t TempHastaA;
    int16_t TempDesdeB;
    int16_t TempHastaB;
    uint16_t TiempoA;
    uint16_t TiempoB;
    DeviceAddress addrRele;
    DeviceAddress addrSensorA;
    DeviceAddress addrSensorB;
    OneWire* _wire;
};