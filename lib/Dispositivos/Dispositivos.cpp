#include <Dispositivos.h>

    Dispositivos::Dispositivos (OneWire* _oneWire)
    {
        _wire = _oneWire;
    }

    Dispositivos::~Dispositivos ()
    {
        delete(this);
    }    
    void Dispositivos::Begin (int16_t pDesdeA,int16_t pHastaA,int16_t pDesdeB,int16_t pHastaB,
                         DeviceAddress Rele, DeviceAddress SensorTempA, 
                         DeviceAddress SensorTempB, uint16_t timeA, uint16_t timeB, bool pEnabled,
                         bool pSensorAEnabled, bool pSensorBEnabled)
    {
        DeviceAddress addrAux;
        TempDesdeA = pDesdeA;
        TempHastaA = pHastaA;
        TempDesdeB = pDesdeB;
        TempHastaB = pHastaB;
        for (uint8_t i = 0; i < 8; i++)
        {
            addrRele[i] = Rele[i];
            addrAux[i] = Rele[i];
            addrSensorA[i] = SensorTempA[i];
            addrSensorB[i] = SensorTempB[i];
        }
        TiempoA = timeA;
        TiempoB = timeB;
        Activado = pEnabled;
        this->EnableSensor("A",pSensorAEnabled);
        this->EnableSensor("B",pSensorBEnabled);
        //SensorAactivado = pSensorAEnabled;
        //SensorBactivado = pSensorBEnabled;

        if (Activado)
        {
//            Conectado = true;
            //se fuerza que conectado esté en true porque la línea de abajo está mal. El search busca nuevos dispositivos. Revisar qué hace el select.
            //Conectado = _wire->search(addrAux);//es necesario buscar por el dispositivo. Se comenta para evitar el error de parámetro por referencia.
//            if (SensorAactivado)
//            {
                this->SensorTempA.setOneWire(_wire);
                this->SensorTempA.begin();

                //Se comenta la siguiente línea porque está mal usado el search, ya que el mismo se usa para nuevas conexiones.
                //SensorAPresente = _wire->search(addrAux);//es necesario buscar por el dispositivo. Se comenta para evitar el error de parámetro por referencia.

//            }
//            if (SensorBactivado)
//           {
                this->SensorTempB.setOneWire(_wire);
                this->SensorTempB.begin();

                //Se comenta la siguiente línea porque está mal usado el search, ya que el mismo se usa para nuevas conexiones.
                //SensorBPresente = _wire->search(addrAux);//es necesario buscar por el dispositivo. Se comenta para evitar el error de parámetro por referencia.
//            }
//            if (Conectado)
//            {
                //OneWireSwitch ctl2406 = new OneWireSwitch(&_wire,&addrRele);
                ctl2406.Begin(_wire,&addrRele[0]);
                //Se inicializa el estado a false porque tambien se hacen con EstadoA y EstadoB
                this->ctl2406.setSwitchState(false,false);
//            }
        }
    }

    bool Dispositivos::IsPresent()
    {
        return this->Conectado;
    }

    void Dispositivos::Enable()
    {
        this->Activado = true;
    }

    void Dispositivos::EnableSensor(const char* Sensor, bool pEstado)
    {
        if (Sensor=="A")
            this->SensorAactivado=pEstado;
        else
            this->SensorBactivado=pEstado;
    }

    int16_t Dispositivos::getTempDesde(const char* Sensor)
    {
        if (Sensor=="A")
            return TempDesdeA;
        else
            return TempDesdeB;
    }
    int16_t Dispositivos::getTempHasta(const char* Sensor)
    {
        if (Sensor=="A")
            return TempHastaA;
        else
            return TempHastaB;
    }
        
    void Dispositivos::setTempDesde(const char* Sensor, int16_t pTemp)
    {
        if (Sensor=="A")
            TempDesdeA = pTemp;
        else
            TempDesdeB = pTemp;
    }

    void Dispositivos::setTempHasta(const char* Sensor, int16_t pTemp)
    {
        if (Sensor=="A")
            TempHastaA = pTemp;
        else
            TempHastaB = pTemp;
    }
    
    void Dispositivos::getAddrRele(uint8_t* paddrRele)
    {
        for (uint8_t i = 0; i < 8; i++)
        {
            paddrRele[i] = this->addrRele[i];
        }
    }

    void Dispositivos::getAddrSensor(uint8_t* paddrSensor, const char* Sensor)
    {
        for (uint8_t i = 0; i < 8; i++)
            if (Sensor=="A")
                paddrSensor[i] = this->addrSensorA[i];
            else
                paddrSensor[i] = this->addrSensorB[i];
    }

/*    void Dispositivos::setAddrRele(DeviceAddress *pAddr)
    {
        this->addrRele = pAddr;
    }

    void Dispositivos::setAddrSensor(const char* Sensor, DeviceAddress *pAddr)
    {
        if (Sensor=="A")
            this->addrSensorA = pAddr;
        else
            this->addrSensorB = pAddr;
    }
*/
    uint16_t Dispositivos::getTimeSensor(const char* Sensor)
    {
        if (Sensor=="A")
            return this->TiempoA;
        else
            return this->TiempoB;
    }

    void Dispositivos::setTimeSensor(const char* Sensor, uint16_t Tiempo)
    {
        if (Sensor=="A")
            this -> TiempoA = Tiempo;
        else
            this -> TiempoB = Tiempo;
    }
