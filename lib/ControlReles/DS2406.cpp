#include <DS2406.h>

OneWireSwitch::OneWireSwitch(){}

OneWireSwitch::OneWireSwitch(OneWire *b, uint8_t *addr) {
    this->Begin(b,addr);
}

OneWireSwitch::~OneWireSwitch()
    {
        delete(this);
    }

void OneWireSwitch::Begin(OneWire *b, uint8_t *addr) {
    bus = b;
    for(int i = 0; i<8; i++) {
        address[i] = addr[i];
    }
}

bool OneWireSwitch::getSwitchState(int port) {
    uint8_t status[DS2406_STATE_BUF_LEN];
    readStatus(status);

    Serial.print("Status: ");
    Serial.print(status[7]);
    Serial.println("");
    return (status[7] & port) == 0;
}

void OneWireSwitch::setSwitchState(bool pio_a, bool pio_b) {
    uint8_t state = (!pio_a << 5) | (!pio_b << 6) | 0xf;
    //Eliminar desde acá
    Serial.println("");
      if (pio_a)
        Serial.println("pio_a Activado");
      else
        Serial.println("pio_a Desactivado");
      if (pio_b)
        Serial.println("pio_b Activado");
      else
        Serial.println("pio_b Desactivado");   
    Serial.print("Address Dispositivo: "); 
    for (uint8_t i = 0; i < 8; i++)
    {
        if (address[i] < 16) Serial.print("0");
        Serial.print(address[i], HEX);
    }
    //Eliminar hasta acá
    Serial.println("");
    bus->reset();
    bus->select(address);
    bus->write(DS2406_WRITE_STATUS);
    bus->write(0x07);
    bus->write(0x0);

    bus->write(state);
    // Read the CRC data
    for (int i = 0; i < 6; i++) {
        bus->read();
    }
    // Write the status back.
    bus->write(0xFF);
    bus->reset();

}

//Modificado para que sea funcional 
void OneWireSwitch::readStatus(uint8_t *buffer) {
    bus->reset();
    bus->select(address);
    bus->write(DS2406_READ_STATUS, 1);
    bus->write(0, 1);
    bus->write(0, 1);

    for(int i = 0; i<DS2406_STATE_BUF_LEN; i++) {
        buffer[i] = bus->read();
    }
    bus->reset();
}
