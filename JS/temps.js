const toCent = 0.78125;
var Temps = [[]];
var TempsCent = [[]];
var TempsOnly = [[]];
var i;
function populateTable()
{
    $.get("getTemp", function(respuestaSolicitud){
            console.log("ok");
        Temps = JSON.parse(respuestaSolicitud);
        for (i = 0; i < Temps.length; i++)
    TempsCent[i] = [Temps[i][0],Temps[i][1],Temps[i][2],Temps[i][3],Math.round(Temps[i][4]*toCent)/100,Math.round(Temps[i][5]*toCent)/100,Temps[i][6],Temps[i][7],0];
createTable(TempsCent,"DispTbody");
    }).fail(function(reason){
        alert( "Error al cargar." );
createTable(TempsCent,"DispTbody");
    });
}

function createTable(tableData,tblBody) 
{
    tableBody = document.getElementById(tblBody);
	j=0;
    tableData.forEach(function(rowData) {
      row = document.createElement('tr');
      i=0;
      rowData.forEach(function(cellData) {
         cell = document.createElement('td');
		var txtData = document.createElement("input");
        if (i==0 || i==4 || i==5 || i==6 || (i==7)) {
            txtData.setAttribute('type', 'number');
        }
        if (i==8)
        {
            cell.style.textAlign =  'right';
            cell.innerHTML = cellData;
        }
        else
        {
            txtData.value=cellData;
            txtData.style.textAlign = 'right';
            cell.appendChild(txtData);
        }
        if (i==1||i==2)
            txtData.style.width = '150px';
        else
            txtData.style.width = '50px';
        row.appendChild(cell);
       i=i+1;
      });
	  //if (j==1)
		//row.setAttribute('hidden', 'true');
      tableBody.appendChild(row);
	  j++;
    });
  }

  function enviaTemps(){
    var str1="";
$.ajaxSetup({
  contentType: "application/json; charset=utf-8"
});
    $("tr", "#DispTbody").each((i,row)=>{
        $("td", row).each((i2,col)=>{
            $("input", col).each((dummy,cell)=>{
        Temps[i][i2]=cell.value;
                });
            });
        });
        str1=JSON.stringify(Temps);
        str1.replace(new RegExp('\"'),'');
        $.ajax({
        type : 'POST',
        url : 'setTemp',
        dataType : 'application/json',
        data: str1,
        success: function(res){
        alert(res);
        }
        });

}

function populateTempsOnly()
{
    $.get("getTOnly", function(respuestaSolicitud){
            console.log("ok");
        TempsOnly = JSON.parse(respuestaSolicitud);
        for (i = 0; i < TempsOnly.length; i++)
    TempsCent[i][8] = [Math.round(TempsOnly[i]*toCent)/100];
fillTableTemps(TempsCent,"DispTbody");
    }).fail(function(reason){
        alert( "Error al cargar las temperaturas." );
fillTableTemps(TempsCent,"DispTbody");
    })
}

function fillTableTemps(tableData,tblBody) {
    i=0;
    tableBody = document.getElementById(tblBody);
    tableData.forEach(function(rowData) {
      //row = document.createElement('tr');
		rowData.forEach(function(cellData) {
        //cell = document.getElementById("thTempActual");
        //cell.innerHTML = cellData;
        document.getElementById(tblBody).rows[i].cells[8].innerHTML = cellData;
        });
    i=i+1;
    });
  }