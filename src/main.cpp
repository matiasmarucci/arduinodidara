#include <Arduino.h>
#include <OneWire.h>
#include <ControlTemperature.h>
#include <WiFi.h>
#include <EEPROM.h>
#include <Dispositivos.h>
#define DEBUGTEMP true 
#define DEBUGSENSOR true
#define WIFIPRESENT true
#define ONE_WIRE_BUS 4

void printAddress(DeviceAddress deviceAddress);
char *addr2str(DeviceAddress deviceAddress);
//void printAlarms(uint8_t deviceAddress[]);
void discoverOneWireDevices(void);
bool setTemps(void);

OneWire oneWire(ONE_WIRE_BUS);

// Data wire is plugged into port 2 on the Arduino

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)

uint8_t cantDisp;
DeviceAddress AddrAux;
//ControlTemperature ctlAux(&oneWire);

Dispositivos *ctlFermentador[32];
#include <servercontent.h>
String strTemps;
const float toCent = .78125;

unsigned long TiempoChillerApagado;
#if WIFIPRESENT
const char* ssid     = "Fibertel WiFi724";
const char* password = "0043370005";
//const char* ssid     = "romicostas";
//const char* password = "romi0220";
//const char* ssid     = "AndroidAP";
//const char* password = "oracle101";
#endif

 struct sctFermentador {
    int16_t TempDesdeA;
    int16_t TempHastaA;
    int16_t TempDesdeB;
    int16_t TempHastaB;
    uint16_t TimeSensorA;
    uint16_t TimeSensorB;
    DeviceAddress addrRele;
    DeviceAddress addrSensorA;
    DeviceAddress addrSensorB;
    bool Enabled;
    bool SAEnabled;
    bool SBEnabled;
  } FermAux;

#if WIFIPRESENT

WiFiServer server(80);

void connectWifiAndStartServer();
void listenClients();

void connectWifiAndStartServer() {
  int i=0;
    #if DEBUGTEMP
    Serial.println();
    Serial.println();
    Serial.print("Conectando 3 a ");
    Serial.println(ssid);
    #endif
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED && i<60) {
        delay(500);
        #if DEBUGTEMP
        Serial.print(".");
        #endif
        i++;
    }
    #if DEBUGTEMP 
    if (i<59)
    {
      Serial.println("");
      Serial.println("WiFi Conectado.");
      Serial.println("direccion IP: ");
      Serial.println(WiFi.localIP());
    }
    else
    {
      Serial.println("");
    }
    #endif
    server.begin();
}
#endif

uint8_t str2addr (String str)
  {
    if (str=="A")
      return 10;
    else if (str=="B")
      return 11;
    else if (str=="C")
      return 12;
    else if (str=="D")
      return 13;
    else if (str=="E")
      return 14;
    else if (str=="F")
      return 15;
    else
      return str.toInt();
  }

char *addr2str(DeviceAddress deviceAddress)
{
    static char return_me[18];
    static char *hex = "0123456789ABCDEF";
    uint8_t i, j;
    for (i=0, j=0; i<8; i++) 
    {
         return_me[j++] = hex[deviceAddress[i] / 16];
         return_me[j++] = hex[deviceAddress[i] & 15];
    }
    return_me[j] = '\0';
    return (return_me);
}

void getTemps(String &strAux)
{
  strAux="[";
  uint16_t iAux;
  DeviceAddress AddrDispAux;
  for (uint8_t i=0;i<cantDisp;i++) //el 0 reservado para la cantidad configurada
  {
    //Sensor A
    if (i>0)
      strAux = strAux + ",";
    ctlFermentador[i]->getAddrRele(AddrDispAux);
    strAux = strAux + "[" + i + ",\"" + addr2str(AddrDispAux) + "\",\"";
    ctlFermentador[i]->getAddrSensor(AddrDispAux,"A"); 
    strAux = strAux + addr2str(AddrDispAux) + "\",\"A\","; 
    iAux = ctlFermentador[i]->getTempDesde("A"); 
    strAux = strAux + iAux + ","; 
    iAux = ctlFermentador[i]->getTempHasta("A"); 
    strAux = strAux + iAux + ","; 
    iAux = ctlFermentador[i]->getTimeSensor("A"); 
    strAux = strAux + iAux + ","; 
    if (ctlFermentador[i]->SensorAactivado)
      strAux = strAux + "1";
    else
      strAux = strAux + "0";
    strAux = strAux + "],["; 
    //Sensor B
    ctlFermentador[i]->getAddrRele(AddrDispAux);
    strAux = strAux + i + ",\"" + addr2str(AddrDispAux) + "\",\"";
    ctlFermentador[i]->getAddrSensor(AddrDispAux,"B"); 
    strAux = strAux + addr2str(AddrDispAux) + "\",\"B\","; 
    iAux = ctlFermentador[i]->getTempDesde("B"); 
    strAux = strAux + iAux + ","; 
    iAux = ctlFermentador[i]->getTempHasta("B"); 
    strAux = strAux + iAux + ","; 
    iAux = ctlFermentador[i]->getTimeSensor("B"); 
    strAux = strAux + iAux + ",";
    if (ctlFermentador[i]->SensorBactivado)
      strAux = strAux + "1";
    else
      strAux = strAux + "0";
    strAux = strAux + "]"; 
  } 
  strAux = strAux + "]";
  #if DEBUGTEMP
    Serial.print("strAux: ");
    Serial.print(strAux);
    Serial.println("Final /getTemp");
  #endif
}

void getTempsOnly(String &strAux)
{
  strAux="[";
  for (uint8_t i=0;i<cantDisp;i++) 
  {
    strAux = strAux + ctlFermentador[i]->TempActualA + ",";
    strAux = strAux + ctlFermentador[i]->TempActualB + ",";
  }
  strAux = strAux.substring(0,strAux.length()-1);
  strAux=strAux+"]";
  #if DEBUGTEMP
    Serial.print("strAux: ");
    Serial.print(strAux);
    Serial.println("Final getTOnly");
  #endif
}

#if WIFIPRESENT

void listenClients()
{
  String strResponse;
  bool bPOST=false;
  WiFiClient client = server.available();   // listen for incoming clients
   
  if (client) {                             // if you get a client,
    #if DEBUGTEMP
    Serial.println("Nuevo Cliente.");           // print a message out the serial port
    #endif
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        #if DEBUGTEMP
        Serial.write(c);                    // print it out the serial monitor
        #endif

        if (c == '\n') {
          if (currentLine.length() == 0 && !bPOST) {
            client.println("HTTP/1.1 200 OK");
            client.println("Access-Control-Allow-Origin: *");
            client.println();   
            client.print(strResponse);
            break;
          }
          else 
            currentLine = "";
        }
        else if (c != '\r') {
          currentLine += c;
        }
        if (currentLine.endsWith("GET /getTemp"))
        {
          getTemps(strTemps);
          strResponse=strTemps;
        } 
        else if (currentLine.endsWith("POST /setTemp")) {
          bPOST = true;
          #if DEBUGTEMP
          Serial.println("pidio post settemp");
          #endif
        }
        else if (currentLine.endsWith("GET /getTOnly"))
        {
          getTempsOnly(strTemps);
          strResponse=strTemps;
        } 
          else if (currentLine.endsWith("GET /config.html")) {
          strResponse = ServerConfigHTML;
          #if DEBUGTEMP
          Serial.println("pidio config.html");
          #endif
        }
        else if (currentLine.endsWith("GET /temps.js")) {
          strResponse = ServerJS;
          #if DEBUGTEMP
          Serial.println("pidio temp.js");
          #endif
        }
        else if (currentLine.endsWith("GET /style.css")) {
          strResponse = ServerCSS;
          #if DEBUGTEMP
          Serial.println("pidio style.css");
          #endif
        }
        else if (currentLine.endsWith("]]") && bPOST){
          bPOST=false;
          strTemps=currentLine;
          Serial.print("Llamo2 a settemps2");
          if (setTemps()) {
            #if DEBUGTEMP
            Serial.println("Llamo a setTemps desde listenclients");
            #endif
            client.println("HTTP/1.1 200 OK");
          }
          else {
            client.println("HTTP/1.1 500 Error");
          }
          client.println();   
          client.print(strResponse);
          break;
        }
      }
    }
    // close the connection:
    client.stop();
    
    #if DEBUGTEMP
    Serial.println("Cliente Desconectado.");
    #endif
 
  Serial.println();
  delay(100);

  }
}
#endif

// function to print a device address
#if DEBUGTEMP
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}
#endif

void rstTemps(bool pNoBegin)
{
  //for (uint8_t i=0;i<cantDisp;i++)
    //delete &ctlFermentador[i];
  
  EEPROM.begin(2);
  
  //EEPROM.put(0,AddrSensorTemp[0]);
  //EEPROM.write(0,193);
  //EEPROM.commit();
  EEPROM.get(0,cantDisp); //el 0 reservado para la cantidad configurada
  //FermAux sctFermentador;
  cantDisp = 3;
  EEPROM.begin(cantDisp*sizeof(FermAux)+1);

  #if DEBUGTEMP
    Serial.print(F("Cantidad de 2406: "));
    Serial.println(cantDisp);
    Serial.println("Antes de la inicializacion");
    Serial.print(F("Addr 2406: "));
    printAddress(FermAux.addrRele);
    Serial.print(F("Sensor A: "));
    printAddress(FermAux.addrSensorA);
    Serial.println(" ");
    Serial.print(F("Sensor B: "));
    printAddress(FermAux.addrSensorB);
    Serial.println(" ");
    if (FermAux.SAEnabled)
      Serial.println("Sensora A Activado");
    else
      Serial.println("Sensora A Desactivado");
    if (FermAux.SBEnabled)
      Serial.println("Sensora B Activado");
    else
      Serial.println("Sensora B Desactivado");
  #endif
  Serial.println("Antes del for del EPROM");
  for (uint8_t i=0;i<cantDisp;i++) //el 0 reservado para la cantidad configurada
  {
    EEPROM.get((i*sizeof(FermAux))+1,FermAux);
    if (pNoBegin)
    {
      ctlFermentador[i]->setTempDesde("A",FermAux.TempDesdeA);
      ctlFermentador[i]->setTempHasta("A",FermAux.TempHastaA);
      ctlFermentador[i]->setTempDesde("B",FermAux.TempDesdeB);
      ctlFermentador[i]->setTempHasta("B",FermAux.TempHastaB);
      ctlFermentador[i]->setTimeSensor("A",FermAux.TimeSensorA);
      ctlFermentador[i]->setTimeSensor("B",FermAux.TimeSensorB);
      ctlFermentador[i]->EnableSensor("A",FermAux.SAEnabled);
      ctlFermentador[i]->EnableSensor("B",FermAux.SBEnabled);
    }
    else
    {    
      ctlFermentador[i] = new Dispositivos(&oneWire);
      ctlFermentador[i]->Begin(FermAux.TempDesdeA,FermAux.TempHastaA,FermAux.TempDesdeB,
                                FermAux.TempHastaB, FermAux.addrRele,FermAux.addrSensorA,
                                FermAux.addrSensorB,FermAux.TimeSensorA,FermAux.TimeSensorB,
                                FermAux.Enabled, FermAux.SAEnabled, FermAux.SBEnabled);
    }
  
  #if DEBUGTEMP
    Serial.print(F("Dispositivo Iniciado. DS2406: "));
    ctlFermentador[i]->getAddrRele(AddrAux);
    printAddress(AddrAux);
    Serial.println(" ");
    Serial.print(F("Sensor A: "));
    ctlFermentador[i]->getAddrSensor(AddrAux,"A");
    printAddress(AddrAux);
    Serial.println(" ");
    Serial.print(F("Sensor B: "));
    ctlFermentador[i]->getAddrSensor(AddrAux,"B");
    printAddress(AddrAux);
    Serial.println(" ");
    Serial.print(F("Tiempo A: "));
    Serial.println(ctlFermentador[i]->getTimeSensor("A"));
    Serial.println(" ");
    Serial.print(F("Tiempo B: "));
    Serial.println(ctlFermentador[i]->getTimeSensor("B"));
    Serial.println(" ");

    Serial.print(F("TempDesde A: "));
    Serial.println(ctlFermentador[i]->getTempDesde("A"));
    Serial.println(" ");
    Serial.print(F("TempHasta A: "));
    Serial.println(ctlFermentador[i]->getTempHasta("A"));
    Serial.println(" ");
    Serial.print(F("TempDesde B: "));
    Serial.println(ctlFermentador[i]->getTempDesde("B"));
    Serial.println(" ");
    Serial.print(F("TempHasta B: "));
    Serial.println(ctlFermentador[i]->getTempHasta("B"));
    Serial.println(" ");
    Serial.print(F("Sensor A: "));
    if (ctlFermentador[i]->SensorAactivado)
      Serial.println("Activado");
    else
      Serial.println("Desactivado");
    Serial.println(" ");
    Serial.print(F("Sensor B: "));
    if (ctlFermentador[i]->SensorBactivado)
      Serial.println("Activado");
    else
      Serial.println("Desactivado");
    Serial.println(" ");
  #endif  
  }
  EEPROM.end(); 

}

void setup(void)
{
  // start serial port
  //pinMode(ONE_WIRE_BUS,INPUT_PULLUP);
  #if DEBUGTEMP
    Serial.begin(9600);
    Serial.println("Control de Fermentadores.");
  #endif
  discoverOneWireDevices();
  rstTemps(false);
  TiempoChillerApagado = millis();

  delay(250);

#if WIFIPRESENT
  connectWifiAndStartServer();
#endif
}


void loop(void)
{
  int16_t tempA;
  int16_t tempB;
  bool pioA;
  bool pioB;
  bool EstadoBomba;
  bool EstadoChiller=false;
  unsigned long CurrentMillis;
  delay(1000);
  ctlFermentador[0]->SensorTempA.requestTemperatures(); //No es necesario hacerlo para el resto ya que el comando hace el request para todos los sensores
  EstadoBomba = false;
    for (uint8_t i=0;i<cantDisp;i++) //el 0 es el de la bomba.
    {
      #if DEBUGTEMP 
      Serial.println(" ");
      if (ctlFermentador[i]->SensorAactivado)
        Serial.println("######## Sensor A Activado");
      else
        Serial.println("######## Sensor A Desactivado");
      Serial.println(" ");
      #endif
      if (ctlFermentador[i]->SensorAactivado)
      {
        ctlFermentador[i]->getAddrSensor(AddrAux,"A");
        #if DEBUGTEMP
          if (i==0)
            Serial.print(F("Temp. sensor Chiller: "));
          else
            Serial.print(F("Temp. sensor A: "));
          printAddress(AddrAux);
          Serial.println(" ");
        #endif
        portDISABLE_INTERRUPTS();
        tempA = ctlFermentador[i]->SensorTempA.getTemp(AddrAux);
        portENABLE_INTERRUPTS();
        ctlFermentador[i]->TempActualA = tempA;
        #if DEBUGTEMP
          Serial.print(tempA*toCent/100);
          Serial.println(F(" "));
          #if DEBUGSENSOR
          Serial.print("Temperatura TempC: ");
          portDISABLE_INTERRUPTS();
          Serial.print(ctlFermentador[i]->SensorTempA.getTempC(AddrAux));
          portENABLE_INTERRUPTS();
          Serial.println(F(" "));
          Serial.print("Resolution: ");
          delay(100);
          Serial.print(ctlFermentador[i]->SensorTempA.getResolution(AddrAux));
          Serial.println(F(" "));
          if (ctlFermentador[i]->SensorTempA.readPowerSupply(AddrAux))
            Serial.println("Parasitic.");
          else
            Serial.println("No parasitic.");
          #endif
          Serial.print("Temp. Desde: ");
          Serial.println(ctlFermentador[i]->getTempDesde("A")*toCent/100);
          Serial.print("Temp. Hasta: ");
          Serial.println(ctlFermentador[i]->getTempHasta("A")*toCent/100);        
        #endif 
        if (tempA >= ctlFermentador[i]->getTempHasta("A") && !ctlFermentador[i]->EstadoA)
        {
          #if DEBUGTEMP
              Serial.println("TempA > TempHasta; Sensor A activado; pioA Desactivado");
          #endif
          pioA=true;
        }
        else if (tempA < ctlFermentador[i]->getTempDesde("A") && ctlFermentador[i]->EstadoA)
        {
          #if DEBUGTEMP
            Serial.println("TempA < TempDesde; Sensor A activado; pioA Activado");
          #endif
          pioA=false;
        }
        else
        {
          pioA = ctlFermentador[i]->EstadoA;
        }
      }
      else
      {
        pioA=false;
      }
      #if DEBUGTEMP
      if (ctlFermentador[i]->SensorBactivado)
        Serial.println("######## Sensor B Activado");
      else
        Serial.println("######## Sensor B Desactivado");
      #endif
      if (ctlFermentador[i]->SensorBactivado && i!=0) //El sensor B está desactivado en el Disp. 0 porque es del chiller
      {
        ctlFermentador[i]->getAddrSensor(AddrAux,"B");
        #if DEBUGTEMP
          Serial.print(F("Temp. sensor B: "));
          printAddress(AddrAux);
          Serial.println(" ");
        #endif
        portDISABLE_INTERRUPTS();
        tempB = ctlFermentador[i]->SensorTempB.getTemp(AddrAux);
        portENABLE_INTERRUPTS();
        ctlFermentador[i]->TempActualB = tempB;
        #if DEBUGTEMP
          Serial.print(tempB*toCent/100);
          Serial.println(F(" "));
          #if DEBUGSENSOR
          Serial.print("Temperatura TempC: ");
          portDISABLE_INTERRUPTS();
          Serial.print(ctlFermentador[i]->SensorTempB.getTempC(AddrAux));
          portENABLE_INTERRUPTS();
          Serial.println(F(" "));
          delay(100);
          Serial.print("Resolution: ");
          Serial.print(ctlFermentador[i]->SensorTempB.getResolution(AddrAux));
          Serial.println(F(" "));
          if (ctlFermentador[i]->SensorTempB.readPowerSupply(AddrAux))
            Serial.println("Parasitic.");
          else
            Serial.println("No parasitic.");
          #endif
          Serial.print("Temp. Desde: ");
          Serial.println(ctlFermentador[i]->getTempDesde("B")*toCent/100);
          Serial.print("Temp. Hasta: ");
          Serial.println(ctlFermentador[i]->getTempHasta("B")*toCent/100);        
        #endif
        if (tempB >= ctlFermentador[i]->getTempHasta("B") && !ctlFermentador[i]->EstadoB) // El 64 especifica el pioB
        {
          #if DEBUGTEMP
            Serial.println("TempB > TempHasta");
          #endif
          pioB=true;
        }
        else if (tempB <= ctlFermentador[i]->getTempDesde("B") && ctlFermentador[i]->EstadoB)
        {
          #if DEBUGTEMP
            Serial.println("TempB < TempDesde");
          #endif
          pioB=false;
        }
        else
        {
          pioB = ctlFermentador[i]->EstadoB;
        }
      }
      else
      {
        pioB=false;
      }
      if (i==0)
      {
        EstadoChiller = pioA;
        EstadoBomba = EstadoBomba || pioB;
      }
      else
      {
        EstadoBomba = EstadoBomba || pioA || pioB;
      }
      #if DEBUGTEMP
      if (pioA)
        Serial.println("pioA Activado");
      else
        Serial.println("pioA Desactivado");
      if (pioB)
        Serial.println("pioB Activado");
      else
        Serial.println("pioB Desactivado");
      if (EstadoBomba)
        Serial.println("EstadoBomba Activado");
      else
        Serial.println("EstadoBomba Desactivado");
      if (ctlFermentador[i]->EstadoA)
        Serial.println("EstadoA: Activado");
      else
        Serial.println("EstadoA: Desactivado");
      if (ctlFermentador[i]->EstadoB)
        Serial.println("EstadoB: Activado");
      else
        Serial.println("EstadoB: Desactivado");
      #endif
      if (i>0 && (ctlFermentador[i]->EstadoA != pioA || ctlFermentador[i]->EstadoB != pioB))
      {
        #if DEBUGTEMP
          Serial.println("Cambia estado Switch");
        #endif
        ctlFermentador[i]->ctl2406.setSwitchState(pioA,pioB);
        ctlFermentador[i]->EstadoA = pioA;
        ctlFermentador[i]->EstadoB = pioB;
      }
      delay(100);
      #if DEBUGTEMP
        ctlFermentador[i]->getAddrRele(AddrAux);
        printAddress(AddrAux);
        Serial.println("");
        Serial.println("###############################");
      #endif
    }
    #if DEBUGTEMP
      if (EstadoBomba)
        Serial.println("EstadoBomba Activado");
      else
        Serial.println("EstadoBomba Desactivado");      
    #endif

    //El estado del chiller es desactivado si no se cumplió el tiempo de espera configurado
    CurrentMillis=millis();
    if (EstadoChiller && ((unsigned long)(CurrentMillis-TiempoChillerApagado)<ctlFermentador[0]->getTimeSensor("A")*1000))
      EstadoChiller=false;

    if (ctlFermentador[0]->EstadoA != EstadoChiller || ctlFermentador[0]->EstadoB != EstadoBomba)
    {
      #if DEBUGTEMP
      Serial.println("Cambia estado (Chiller/Bomba)");
      #endif
      //Si el estado del sensor A es activo y el estadoChiller es desactivado, se apagará el chiller y es necesario guardar el tiempo actual
      if (ctlFermentador[0]->EstadoA && !EstadoChiller)
      {
        TiempoChillerApagado=millis();
      }
      ctlFermentador[0]->ctl2406.setSwitchState(EstadoChiller,EstadoBomba);
      ctlFermentador[0]->EstadoA = EstadoChiller;
      ctlFermentador[0]->EstadoB = EstadoBomba;
    }
    #if DEBUGTEMP
    Serial.print("TiempoApagadoChiller: ");
    Serial.println(millis()-TiempoChillerApagado);
    Serial.println("#############################################################################");    
    #endif
    
#if WIFIPRESENT
  listenClients();
#endif
}

bool setTemps(void)
{
uint8_t ii=0;
String tmpAddress;
String vStr;
String strSplit1;
String strSplit2;
strTemps.replace("\"","");
strTemps=strTemps.substring(2,strTemps.length()-2);
#if DEBUGTEMP
Serial.println("Temperaturas:");
Serial.println(strTemps);
#endif
EEPROM.begin(cantDisp*sizeof(FermAux)+1);
  for (int j = 0; j < 6; j++) {

    strSplit1 = strTemps.substring(0,strTemps.indexOf("],["));
    strTemps = strTemps.substring(strTemps.indexOf("],[")+3);
    for (int i = 0; i < 8; i++) { //recorro columnas. Hacer constante para el valor i
      if (i!=7)
      {
        strSplit2 = strSplit1.substring(0,strSplit1.indexOf(","));
        strSplit1 = strSplit1.substring(strSplit1.indexOf(",")+1);
      }
      else
      {
        strSplit2 = strSplit1;
        strSplit1 = strTemps;
      }
      #if DEBUGTEMP
      Serial.println("String1: " + strSplit1);
      Serial.println("String2: " + strSplit2);
      #endif
      switch (i) {
        case 1:
          for (ii=0;ii<16;ii+=2){
            FermAux.addrRele[ii/2] = (str2addr(strSplit2.substring(ii,ii+1)) << 4) | (str2addr(strSplit2.substring(ii+1,ii+2)));
            #if DEBUGTEMP
            Serial.print (FermAux.addrRele[ii/2],HEX);
            Serial.println("");
            #endif
          }
            #if DEBUGTEMP
            Serial.print("Direccion 2406:");
            printAddress(FermAux.addrRele);
            Serial.println("");
            Serial.println("DUMP Addr. 2406: ");
            #endif
          break;
        case 2: 
          tmpAddress = strSplit2; //addrSensor
          break;
        case 3:
          vStr =  strSplit2.substring(0,i);
          if (vStr=="A"){

          for (ii=0;ii<16;ii+=2){
            FermAux.addrSensorA[ii/2] = (str2addr(tmpAddress.substring(ii,ii+1)) << 4) | (str2addr(tmpAddress.substring(ii+1,ii+2)));
          }
            #if DEBUGTEMP
            Serial.print("Dirección Sensor A:");
            printAddress(FermAux.addrSensorA);
            Serial.println("");
            #endif
          }
          else
          {  
            for (ii=0;ii<16;ii+=2){
              FermAux.addrSensorB[ii/2] = (str2addr(tmpAddress.substring(ii,ii+1)) << 4) | (str2addr(tmpAddress.substring(ii+1,ii+2)));
            }
            #if DEBUGTEMP
            Serial.print("Direccion Sensor B:");
            printAddress(FermAux.addrSensorB);
            Serial.println("");
            #endif
          }
          break;
        case 4:
          if (vStr=="A")
          {
            FermAux.TempDesdeA = strSplit2.toFloat() * 100 / toCent;
            #if DEBUGTEMP
            Serial.print("Temp. Desde A:");
            Serial.println(FermAux.TempDesdeA);
            #endif
          }
          else
          {
            FermAux.TempDesdeB = strSplit2.toFloat() * 100 / toCent;
            #if DEBUGTEMP
            Serial.print("Temp. Desde B:");
            Serial.println(FermAux.TempDesdeB);
            #endif
          }
          break;
        case 5:
          if (vStr=="A")
          {
            FermAux.TempHastaA = strSplit2.toFloat() * 100 / toCent;
            #if DEBUGTEMP
            Serial.print("Temp. Hasta A:");
            Serial.println(FermAux.TempHastaA);
            #endif
          }
          else  
          {
            FermAux.TempHastaB = strSplit2.toFloat() * 100 / toCent;
            #if DEBUGTEMP
            Serial.print("Temp. Hasta B:");
            Serial.println(FermAux.TempHastaB);
            #endif
          }
          break;
        case 6:
          if (vStr=="A")
          {
            FermAux.TimeSensorA = strSplit2.toInt();
            #if DEBUGTEMP
            Serial.print("Tiempo A:");
            Serial.println(FermAux.TimeSensorA);
            #endif
          }
          else  
          {
            FermAux.TimeSensorB = strSplit2.toInt();
            #if DEBUGTEMP
            Serial.print("Tiempo B:");
            Serial.println(FermAux.TimeSensorB);
            #endif
          }
          break;
        case 7:
          if (vStr=="A")
          {
            if (strSplit2=="1")
              FermAux.SAEnabled = true;
            else
              FermAux.SAEnabled = false;
            #if DEBUGTEMP
            Serial.print("Sensor A Activo:");
            Serial.println(FermAux.SAEnabled);
            #endif
          }
          else  
          {
            if (strSplit2=="1")
              FermAux.SBEnabled = true;
            else
              FermAux.SBEnabled = false;
            #if DEBUGTEMP
            Serial.print("Sensor B Activo:");
            Serial.println(FermAux.SBEnabled);
            #endif
          }
          break;

      }
    }
    if (vStr=="B")
    {
      FermAux.Enabled=true;
      #if DEBUGTEMP
        Serial.println(sizeof(FermAux));
      #endif
      EEPROM.put(((j-1)/2)*sizeof(FermAux)+1,FermAux);
      delay(200);
      EEPROM.commit();
      delay(200);
      #if DEBUGTEMP
      Serial.println("Guardo FermAux: "); 
      #endif
    }
  }
EEPROM.end();
delay(200);
rstTemps(true);
//ESP.restart();
return true;

}


void discoverOneWireDevices(void)
{
	byte i;
	byte present = 0;
	byte data[12];
	byte addr[8];
	oneWire.reset_search();
	Serial.print("Looking for 1-Wire devices...\n\r");
	while(oneWire.search(addr))
	{
		Serial.print("\n\rFound \'1-Wire\' device with address:\n\r");
		for( i = 0; i < 8; i++)
		{
			//Serial.print("0x");
			if (addr[i] < 16)
			{
				Serial.print('0');
			}
			Serial.print(addr[i], HEX);
			//if (i < 7)
			//{
			//	Serial.print(", ");
			//}
		}
		if ( OneWire::crc8( addr, 7) != addr[7])
		{
			Serial.print("CRC is not valid!\n");
			return;
		}
	}
	Serial.print("\n\r\n\rThat's it.\r\n");
	oneWire.reset_search();
	return;
}
