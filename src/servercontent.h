//const char ServerConfigHTML[] PROGMEM = "<!DOCTYPE html><html><head> <script src=\"https://code.jquery.com/jquery-3.3.1.js\"></script> <script type=\"text/javascript\" src=\"temps.js\"></script> <link rel=\"stylesheet\" href=\"style.css\"></head><body><div id=\"divEnfriador\"><h1 id=\"Title\">Configuracion Enfriador</h1><h3 id=\"Title\">Sensores y Controladores</h3><table id=\"Disptable\"><thead><tr><th id=\"thDisp\">Dispositivo</th><th id=\"thAddrDisp\">Addr. Disp.</th><th id=\"thAddrSensor\">Addr. Sensor</th><th id=\"thPos\">Posicion</th><th id=\"thTempDesde\">Temp. Desde</th><th id=\"thTempHasta\">Temp. Hasta</th><th id=\"thDiff\">Diff Tiempo (segs)</th><th id=\"thEnabled\">Activado</th></tr></thead><tbody id=\"DispTbody\"> <script>populateTable()</script></tbody></table></div><div id=\"divChiller\"><h3 id=\"Title\">Configuracion Chiller + Bomba</h3> <table id=\"Chiller\"><tbody> <tr> <th>Addr. Disp.</th> <th>Sensor IN</th> <th >Sensor OUT</th> </tr><tr> <td><input type=\"text\" id=\"Chiller01\"/></td><td> <table id=\"SensorIN\"> <tbody> <tr> <th>Temp. Desde</th> <th>Temp. Hasta</th> </tr><tr> <td><input type=\"text\" id=\"Chiller11\"/></td><td><input type=\"text\" id=\"Chiller12\"/></td></tr></tbody> </table> </td><td> <table id=\"SensorOUT\"> <tbody> <tr> <th>Temp. Desde</th> <th>Temp. Hasta</th> </tr><tr> <td><input type=\"text\" id=\"Chiller21\"/></td><td><input type=\"text\" id=\"Chiller22\"/></td></tr></tbody> </table> </td></tr></tbody></table><script>ConverTemps()</script></div><div id=\"divButtons\"><table><tr><td><button onclick=\"enviaTemps()\">Ok</button></td><td><button onclick=\"window.location.reload()\">Cancelar</button></td></tr></table></div></body></html>";
const char ServerCSS[] PROGMEM = "table{font-family: arial, sans-serif; border-collapse: collapse; width: 10%;}td{border: 1px solid #000000; text-align: left; padding: 8px;}th{border: 1px solid #000000; text-align: left; padding: 8px;background-color: rgb(146, 149, 187);}tr:nth-child(even){background-color: #dddddd;}";
//const char ServerJS[] PROGMEM = "const toCent=0.78125;var Temps=[[]];var Chiller=[[1,\"FFFFFFFF\"],[2,2000,2100],[3,2100,2200]];var TempsCent=[[]];var ChillerTemp=[[]];var i;function populateTable(){$.get(\"getTemp\", function(respuestaSolicitud){console.log(\"ok\"); Temps=JSON.parse(respuestaSolicitud); for (i=0; i < Temps.length; i++) TempsCent[i]=[Temps[i][0],Temps[i][1],Temps[i][2],Temps[i][3],Math.round(Temps[i][4]*toCent)/100,Math.round(Temps[i][5]*toCent)/100,Temps[i][6],Temps[i][7]];createTable(TempsCent,\"DispTbody\");}).fail(function(reason){alert( \"Error al cargar las temperaturas.\" );})}function ConverTemps(){var a=document.getElementById(\"Chiller01\");a.value=Chiller[0][1];a=document.getElementById(\"Chiller11\");a.value=Math.round(Chiller[1][1]* toCent)/100;a=document.getElementById(\"Chiller12\");a.value=Math.round(Chiller[1][2]* toCent)/100;a=document.getElementById(\"Chiller21\");a.value=Math.round(Chiller[2][1]* toCent)/100;a=document.getElementById(\"Chiller22\");a.value=Math.round(Chiller[2][2]* toCent)/100;}function createTable(tableData,tblBody){tableBody=document.getElementById(tblBody); tableData.forEach(function(rowData){row=document.createElement('tr'); i=0; rowData.forEach(function(cellData){cell=document.createElement('td'); var txtData=document.createElement(\"input\"); if (i==0 || i==4 || i==5 || i==6 || i==7){txtData.setAttribute('type', 'number');}txtData.value=cellData; txtData.style.textAlign='right'; cell.appendChild(txtData); row.appendChild(cell); i=i+1;}); tableBody.appendChild(row);});}function enviaTemps(){var str1=\"\";$.ajaxSetup({contentType: \"application/json; charset=utf-8\"}); $(\"tr\", \"#DispTbody\").each((i,row)=>{$(\"td\", row).each((i2,col)=>{$(\"input\", col).each((dummy,cell)=>{Temps[i][i2]=cell.value;});});}); str1=JSON.stringify(Temps); str1.replace(new RegExp('\"'),''); $.ajax({type : 'POST', url : 'setTemp', dataType : 'application/json', data: str1, success: function(res){alert(res);}});}";
//const char ServerConfigHTML[] PROGMEM = "<!DOCTYPE html><html><head> <script src=\"https://code.jquery.com/jquery-3.3.1.js\"></script> <script type=\"text/javascript\" src=\"temps.js\"></script> <link rel=\"stylesheet\" href=\"style.css\"></head><body><div id=\"divEnfriador\"><h1 id=\"Title\">Configuración Enfriador</h1><h3 id=\"Title\">Sensores y Controladores</h3><table id=\"Disptable\"><thead><tr><th id=\"thDisp\">Dispositivo</th><th id=\"thAddrDisp\">Addr. Disp.</th><th id=\"thAddrSensor\">Addr. Sensor</th><th id=\"thPos\">Posición</th><th id=\"thTempDesde\">Temp. Desde</th><th id=\"thTempHasta\">Temp. Hasta</th><th id=\"thDiff\">Diff Tiempo (segs)</th><th id=\"thEnabled\">Activado</th></tr></thead><tbody id=\"DispTbody\"> <script>populateTable()</script></tbody></table></div><div id=\"divButtons\"><table><tr><td><button onclick=\"enviaTemps()\">Ok</button></td><td><button onclick=\"window.location.reload()\">Cancelar</button></td></tr></table></div></body></html>";
//const char ServerConfigHTML[] PROGMEM = "<!DOCTYPE html><html><head> <script src=\"https://code.jquery.com/jquery-3.3.1.js\"></script> <script type=\"text/javascript\" src=\"temps.js\"></script> <link rel=\"stylesheet\" href=\"style.css\"></head><body><div id=\"divEnfriador\"><h1 id=\"Title\">Configuraci%C3%B3n Enfriador</h1><h3 id=\"Title\">Sensores y Controladores</h3><table id=\"Disptable\"><thead><tr><th id=\"thDisp\">Dispositivo</th><th id=\"thAddrDisp\">Addr. Disp.</th><th id=\"thAddrSensor\">Addr. Sensor</th><th id=\"thPos\">Posición</th><th id=\"thTempDesde\">Temp. Desde</th><th id=\"thTempHasta\">Temp. Hasta</th><th id=\"thDiff\">Diff Tiempo (segs)</th><th id=\"thEnabled\">Activado</th></tr></thead><tbody id=\"DispTbody\"> <script>populateTable()</script></tbody></table></div><div id=\"divButtons\"><table><tr><td><button onclick=\"enviaTemps()\">Ok</button></td><td><button onclick=\"window.location.reload()\">Cancelar</button></td></tr></table></div></body></html>";
//const char ServerJS[] PROGMEM = "const toCent=0.78125;var Temps=[[]];var TempsCent=[[]];var i;function populateTable(){$.get(\"getTemp\", function(respuestaSolicitud){console.log(\"ok\"); Temps=JSON.parse(respuestaSolicitud); for (i=0; i < Temps.length; i++) TempsCent[i]=[Temps[i][0],Temps[i][1],Temps[i][2],Temps[i][3],Math.round(Temps[i][4]*toCent)/100,Math.round(Temps[i][5]*toCent)/100,Temps[i][6],Temps[i][7]];createTable(TempsCent,\"DispTbody\");}).fail(function(reason){alert( \"Error al cargar las temperaturas.\" );createTable(TempsCent,\"DispTbody\");})}function createTable(tableData,tblBody){tableBody=document.getElementById(tblBody);j=0; tableData.forEach(function(rowData){row=document.createElement('tr'); i=0;rowData.forEach(function(cellData){cell=document.createElement('td');var txtData=document.createElement(\"input\"); if (i==0 || i==4 || i==5 || i==6 || (i==7)){txtData.setAttribute('type', 'number');}txtData.value=cellData; txtData.style.textAlign='right'; cell.appendChild(txtData); row.appendChild(cell); i=i+1;}); tableBody.appendChild(row); j++;});}function enviaTemps(){var str1=\"\";$.ajaxSetup({contentType: \"application/json; charset=utf-8\"}); $(\"tr\", \"#DispTbody\").each((i,row)=>{$(\"td\", row).each((i2,col)=>{$(\"input\", col).each((dummy,cell)=>{Temps[i][i2]=cell.value;});});}); str1=JSON.stringify(Temps); str1.replace(new RegExp('\"'),''); $.ajax({type : 'POST', url : 'setTemp', dataType : 'application/json', data: str1, success: function(res){alert(res);}});}";

const char ServerJS[] PROGMEM = "const toCent = 0.78125;\n"
"var Temps = [[]];\n"
"var TempsCent = [[]];\n"
"var TempsOnly = [[]];\n"
"var i;\n"
"function populateTable()\n"
"{\n"
"    $.get(\"getTemp\", function(respuestaSolicitud){\n"
"            console.log(\"ok\");\n"
"        Temps = JSON.parse(respuestaSolicitud);\n"
"        for (i = 0; i < Temps.length; i++)\n"
"    TempsCent[i] = [Temps[i][0],Temps[i][1],Temps[i][2],Temps[i][3],Math.round(Temps[i][4]*toCent)/100,Math.round(Temps[i][5]*toCent)/100,Temps[i][6],Temps[i][7],0];\n"
"createTable(TempsCent,\"DispTbody\");\n"
"    }).fail(function(reason){\n"
"        alert( \"Error al cargar.\" );\n"
"createTable(TempsCent,\"DispTbody\");\n"
"    });\n"
"}\n"
"\n"
"function createTable(tableData,tblBody) \n"
"{\n"
"    tableBody = document.getElementById(tblBody);\n"
"\tj=0;\n"
"    tableData.forEach(function(rowData) {\n"
"      row = document.createElement('tr');\n"
"      i=0;\n"
"      rowData.forEach(function(cellData) {\n"
"         cell = document.createElement('td');\n"
"\t\tvar txtData = document.createElement(\"input\");\n"
"        if (i==0 || i==4 || i==5 || i==6 || (i==7)) {\n"
"            txtData.setAttribute('type', 'number');\n"
"        }\n"
"        if (i==8)\n"
"        {\n"
"            cell.style.textAlign =  'right';\n"
"            cell.innerHTML = cellData;\n"
"        }\n"
"        else\n"
"        {\n"
"            txtData.value=cellData;\n"
"            txtData.style.textAlign = 'right';\n"
"            cell.appendChild(txtData);\n"
"        }\n"
"        if (i==1||i==2)\n"
"            txtData.style.width = '150px';\n"
"        else\n"
"            txtData.style.width = '50px';\n"
"        row.appendChild(cell);\n"
"       i=i+1;\n"
"      });\n"
"\t  //if (j==1)\n"
"\t\t//row.setAttribute('hidden', 'true');\n"
"      tableBody.appendChild(row);\n"
"\t  j++;\n"
"    });\n"
"  }\n"
"\n"
"  function enviaTemps(){\n"
"    var str1=\"\";\n"
"$.ajaxSetup({\n"
"  contentType: \"application/json; charset=utf-8\"\n"
"});\n"
"    $(\"tr\", \"#DispTbody\").each((i,row)=>{\n"
"        $(\"td\", row).each((i2,col)=>{\n"
"            $(\"input\", col).each((dummy,cell)=>{\n"
"        Temps[i][i2]=cell.value;\n"
"                });\n"
"            });\n"
"        });\n"
"        str1=JSON.stringify(Temps);\n"
"        str1.replace(new RegExp('\\\"'),'');\n"
"        $.ajax({\n"
"        type : 'POST',\n"
"        url : 'setTemp',\n"
"        dataType : 'application/json',\n"
"        data: str1,\n"
"        success: function(res){\n"
"        alert(res);\n"
"        }\n"
"        });\n"
"\n"
"}\n"
"\n"
"function populateTempsOnly()\n"
"{\n"
"    $.get(\"getTOnly\", function(respuestaSolicitud){\n"
"            console.log(\"ok\");\n"
"        TempsOnly = JSON.parse(respuestaSolicitud);\n"
"        for (i = 0; i < TempsOnly.length; i++)\n"
"    TempsCent[i][8] = [Math.round(TempsOnly[i]*toCent)/100];\n"
"fillTableTemps(TempsCent,\"DispTbody\");\n"
"    }).fail(function(reason){\n"
"        alert( \"Error al cargar las temperaturas.\" );\n"
"fillTableTemps(TempsCent,\"DispTbody\");\n"
"    })\n"
"}\n"
"\n"
"function fillTableTemps(tableData,tblBody) {\n"
"    i=0;\n"
"    tableBody = document.getElementById(tblBody);\n"
"    tableData.forEach(function(rowData) {\n"
"      //row = document.createElement('tr');\n"
"\t\trowData.forEach(function(cellData) {\n"
"        //cell = document.getElementById(\"thTempActual\");\n"
"        //cell.innerHTML = cellData;\n"
"        document.getElementById(tblBody).rows[i].cells[8].innerHTML = cellData;\n"
"        });\n"
"    i=i+1;\n"
"    });\n"
"  }";

const char ServerConfigHTML[] PROGMEM = "<!DOCTYPE html>\n"
"<html>\n"
"<head>\n"
"    <script src=\"https://code.jquery.com/jquery-3.3.1.js\"></script>\n"
"    <script type=\"text/javascript\" src=\"temps.js\"></script> \n"
"<link rel=\"stylesheet\"  href=\"style.css\">\n"
"</head>\n"
"<body>\n"
"<div id=\"divEnfriador\">\n"
"<h1 id=\"Title\">Configuración Enfriador</h1>\n"
"<h3 id=\"Title\">Sensores y Controladores</h3>\n"
"<table id=\"Disptable\">\n"
"<thead>\n"
"<tr>\n"
"<th id=\"thDisp\">Dispositivo *</th>\n"
"<th id=\"thAddrDisp\">Addr. Disp.</th>\n"
"<th id=\"thAddrSensor\">Addr. Sensor</th>\n"
"<th id=\"thPos\">Posición</th>\n"
"<th id=\"thTempDesde\">Temp. Desde</th>\n"
"<th id=\"thTempHasta\">Temp. Hasta</th>\n"
"<th id=\"thDiff\">Diff Tiempo (segs)</th>\n"
"<th id=\"thEnabled\">Activado</th>\n"
"<th id=\"thTempActual\">Temp. Actual</th>\n"
"</tr>\n"
"</thead>\n"
"<tbody id=\"DispTbody\">\n"
"        <script>populateTable()</script>\n"
"        <script>populateTempsOnly()</script>\n"
"</tbody>\n"
"</table>\n"
"</div>\n"
"\n"
"<div id=\"divButtons\">\n"
"<table>\n"
"<tr>\n"
"    <td><button onclick=\"populateTempsOnly()\">Refresh Temp.</button></td>\n"
"    <td><button onclick=\"enviaTemps()\">Ok</button></td>\n"
"<td><button onclick=\"window.location.reload()\">Cancelar</button></td>\n"
"</tr>\n"
"</table>\n"
"<div id=\"divRefs\">\n"
"    <h3>(*) El dispositivo \"0\" se utiliza exclusivamente para el Sensor del Chiller (posición \"A\") y la bomba.</h3>\n"
"    <h3>La posición \"B\" del dispositivo \"0\" no se configura ya que no posee asociado ningún sensor y sólo se comanda la bomba.</h3>\n"
"</div>\n"
"</body>\n"
"</html>";